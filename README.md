Experimenting with NixOS modules on non-NixOS systems.

Might eat your machine.

```bash
sh <(curl -L https://nixos.org/nix/install) --daemon
cp example-flake.nix /etc/nixos/flake.nix
cd /etc/nixos
nix build ".#argonConfigurations.$HOSTNAME.system"
sudo bash result/activate
sudo rm /nix/var/nix/profiles/default
sudo reboot
```
