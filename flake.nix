{
  description = "Experimenting with NixOS modules on non-NixOS systems.";

  outputs = { ... }: {
    lib.argonConfiguration = {pkgs, modules, specialArgs}: {
      system = (import ./eval.nix {
        inherit (pkgs) lib system;
        inherit modules specialArgs;
        nixosModulesPath = "${builtins.toPath pkgs.path}/nixos/modules";
      }).config.argon.build.system;
    };
  };
}
