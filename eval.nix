{
  system ? builtins.currentSystem
, nixosModulesPath ? <nixpkgs/nixos/modules>
, baseModules ? (import ./modules { inherit nixosModulesPath; }).allModules
, modules
, specialArgs ? {}
, lib ? import <nixpkgs/lib>
}:

let
  pkgsModule = rec {
    _file = ./eval.nix;
    key = _file;
    config = {
      # Explicit `nixpkgs.system` or `nixpkgs.localSystem` should override
      # this.  Since the latter defaults to the former, the former should
      # default to the argument. That way this new default could propagate all
      # they way through, but has the last priority behind everything else.
      nixpkgs.system = lib.mkDefault system;

      # _module.args.pkgs = lib.mkIf (pkgs_ != null) (lib.mkForce pkgs_);
    };
  };
  extraModules = [];
  modulesModule.config._module.args = {
    inherit baseModules extraModules modules;
  };
in rec {
  inherit (lib.evalModules {
    specialArgs = specialArgs // { modulesPath = nixosModulesPath; };
    modules = baseModules ++ modules ++ [ pkgsModule modulesModule ];
  }) config options _module;

  inherit (_module.args) pkgs;
}
