set -euo pipefail
from="$1"
static="/etc/static"

atomicSymlink() {
    local tmp="$2.tmp"
    rm -f "$tmp"
    ln -s "$1" "$tmp"
    mv -T "$tmp" "$2" || true
}

atomicSymlink "$from" "$static"

# Remove dangling symlinks that point to /etc/static
cleanup() {
    local target=$(readlink "$1")
    if [ "${target::${#static}}" = "$static" -a -L "$target" ]; then
        # >&2 echo removing obsolete symlink "$1"...
        rm "$1"
    fi
}

(find /etc -type l -print0 || true) | while IFS= read -r -d '' file; do cleanup "$file"; done

warn() {
    >&2 echo "warning: $@ ($?)"
}

# Returns 0 if the argument points to the files in /etc/static.  That
# means either argument is a symlink to a file in /etc/static or a
# directory with all children being static.
isStatic() {
    if [ -L "$1" ]; then
        local target="$(readlink $1)"
        if [ "${target::${#static}}" = "$static" ]; then
            return 0
        else
            return 1
        fi
    fi

    if [ -d "$1" ]; then
        while IFS= read -r -d '' file; do
            if ! isStatic "$file"; then
                return 1
            fi
        done < <(find "$1" -mindepth 1 -print0 || true)
        return 0
    fi

    return 1
}

link() {
    local fn="${1:${#from}}"
    local target="/etc/$fn"
    mkdir -p "$(dirname "$target")"

    if [ -d "$target" ]; then
        if isStatic "$target"; then
            rm -r "$target" || warn "could not remove $target"
        else
            warn "$target directory contains user files. Symlink will fail."
        fi
    elif [ -e "$target" ] && ! isStatic "$target"; then
        warn "$target already exists. Copying to /etc/argon-moved before overwrite."
        mkdir -p "$(dirname "/etc/argon-moved/$fn")"
        cp -ar "/etc/$fn" "/etc/argon-moved/$fn"
    fi

    atomicSymlink "$static/$fn" "$target" || warn "could not symlink $target"
}

find "$from" -type l -print0 | while IFS= read -r -d '' file; do link "$file"; done
find -L /etc -lname "$static/*" -delete
