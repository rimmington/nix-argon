{config, lib, pkgs, ...}:

with lib;

let
  cfg = config.argon.inhibit;
  tru = mkOption {
    type = types.bool;
    default = true;
  };
in {
  options.argon.inhibit = {
    etcSources = mkOption {
      type = types.listOf types.path;
      default = [];
    };
    specialMounts = mkOption {
      type = types.listOf types.path;
    };

    man-db = tru;
    shared-mime-info = tru;
    systemd = tru;
  };

  config = {
    argon.inhibit = {
      specialMounts = [
        "/proc"
        "/run"
        "/dev"
        "/dev/shm"
        "/dev/pts"
        "/sys"
      ];
      etcSources = mkIf cfg.systemd [
        "${pkgs.systemd}/example"
      ];
    };

    argon.systemPackages = {
      man-db = mkIf cfg.man-db (mkOverride 90 []);
      shared-mime-info = mkIf cfg.shared-mime-info (mkOverride 90 []);
      systemd = mkIf cfg.systemd (mkOverride 90 []);
    };

    environment.etc = {
      "lsb-release".enable = false;
      "os-release".enable = false;
      "protocols".enable = false;
      "rpc".enable = false;
      "services".enable = false;
      "udev/hwdb.bin".enable = false;
      "udev/rules.d".enable = false;
    };

    environment.profiles = mkOrder 1600 [ "/usr" ];
    environment.profileRelativeSessionVariables.PATH = mkBefore [ "/sbin" ];

    environment.shells = [ "/bin/bash" ];  # Avoid breaking existing users

    systemd.suppressedSystemUnits = mkIf cfg.systemd [
      "systemd-backlight@.service"
      "systemd-fsck@.service"
      "systemd-importd.service"
      "systemd-journal-flush.service"
      "systemd-journal-gatewayd.socket"
      "systemd-journald.service"
      "systemd-logind.service"
      "systemd-random-seed.service"
      "systemd-remount-fs.service"
      "systemd-sysctl.service"
      "systemd-udev-settle.service"
      "systemd-udevd.service"
      "systemd-update-utmp.service"
      "systemd-user-sessions.service"
      "user-runtime-dir@.service"
      "user@.service"
    ];
  };
}
