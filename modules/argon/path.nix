{config, lib, pkgs, ...}:

with lib;

{
  options.argon = {
    build.path = mkOption {
      type = types.package;
    };

    systemPackages = mkOption {
      type = types.attrsOf (types.listOf types.package);
    };
  };

  config.argon = {
    systemPackages = zipAttrs
      (map (p: { "${p.pname or (getName p.name)}" = p; }) config.environment.systemPackages);

    # from nixos system-path.nix
    build.path = pkgs.buildEnv {
      name = "system-path";
      paths = concatLists (attrValues config.argon.systemPackages);
      inherit (config.environment) pathsToLink extraOutputsToInstall;
      ignoreCollisions = true;
      # !!! Hacky, should modularise.
      # outputs TODO: note that the tools will often not be linked by default
      postBuild =
        ''
          # Remove wrapped binaries, they shouldn't be accessible via PATH.
          find $out/bin -maxdepth 1 -name ".*-wrapped" -type l -delete

          if [ -x $out/bin/glib-compile-schemas -a -w $out/share/glib-2.0/schemas ]; then
              $out/bin/glib-compile-schemas $out/share/glib-2.0/schemas
          fi

          ${config.environment.extraSetup}
        '';
    };
  };
}
