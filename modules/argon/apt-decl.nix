{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.apt-decl;
  wantsFile = pkgs.writeText "wants" (
    optionalString (cfg.packagesFile != null) (builtins.readFile cfg.packagesFile) +
    lib.concatStringsSep "\n" cfg.systemPackages
  );
in {
  options.apt-decl = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Enable semi-declarative apt package management.
      '';
    };

    packagesFile = mkOption {
      type = types.nullOr types.path;
      default = null;
      description = ''
        File containing newline-separated package names.
      '';
    };

    systemPackages = mkOption {
      type = types.listOf types.str;
      default = [];
      description = ''
        List of package names wanted.
      '';
    };
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = cfg.enable -> (cfg.systemPackages != [] || cfg.packagesFile != null);
        message = "packagesFile or systemPackages must be set.";
      }
    ];

    argon.activationScripts.apt-decl = {
      deps = [ "users" ];
      text = ''
        PATH=$PATH:/usr/bin bash ${../../apt-decl/apt-decl} apply ${wantsFile}
      '';
    };
  };
}
