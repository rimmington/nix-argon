{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.argon;

  # from nixos activation-script.nix
  addAttributeName = mapAttrs (a: v: v // {
    text = ''
      #### Activation script snippet ${a}:
      _localstatus=0
      ${v.text}

      if (( _localstatus > 0 )); then
        printf "Activation script snippet '%s' failed (%s)\n" "${a}" "$_localstatus"
      fi
    '';
  });

  systemActivationScript = set: onlyDry: let
    set' = mapAttrs (_: v: if isString v then (noDepEntry v) // { supportsDryActivation = false; } else v) set;
    withHeadlines = addAttributeName set';
    # When building a dry activation script, this replaces all activation scripts
    # that do not support dry mode with a comment that does nothing. Filtering these
    # activation scripts out so they don't get generated into the dry activation script
    # does not work because when an activation script that supports dry mode depends on
    # an activation script that does not, the dependency cannot be resolved and the eval
    # fails.
    withDrySnippets = mapAttrs (a: v: if onlyDry && !v.supportsDryActivation then v // {
      text = "#### Activation script snippet ${a} does not support dry activation.";
    } else v) withHeadlines;
  in
    ''
      #!${pkgs.runtimeShell}

      systemConfig='@out@'

      export PATH=/empty
      for i in ${toString path}; do
          PATH=$PATH:$i/bin:$i/sbin
      done

      _status=0
      trap "_status=1 _localstatus=\$?" ERR

      # Ensure a consistent umask.
      umask 0022

      ${textClosureMap id (withDrySnippets) (attrNames withDrySnippets)}

    '' + optionalString (!onlyDry) ''
      # Make this configuration the current configuration.
      # The readlink is there to ensure that when $systemConfig = /system
      # (which is a symlink to the store), /run/current-system is still
      # used as a garbage collection root.
      ln -sfn "$(readlink -f "$systemConfig")" /run/current-system

      # Link this configuration for next boot
      mkdir -m 0755 -p /var/argon
      ln -sfn "$(readlink -f "$systemConfig")" /var/argon/next-system

      exit $_status
    '';

  path = with pkgs; map getBin
    [ coreutils
      gnugrep
      findutils
      getent
      stdenv.cc.libc # nscd in update-users-groups.pl
      shadow
      nettools # needed for hostname
      utillinux # needed for mount and mountpoint
    ];

  # From nixos top-level.nix
  systemBuilder =
    ''
      mkdir $out

      ln -s ${config.argon.build.etc}/etc $out/etc
      ln -s ${config.argon.build.path} $out/sw
      ln -s "$systemd" $out/systemd

      echo -n "systemd ${toString config.systemd.package.interfaceVersion}" > $out/init-interface-version
      echo -n "$nixosLabel" > $out/nixos-version
      echo -n "${pkgs.stdenv.hostPlatform.system}" > $out/system

      ${config.system.systemBuilderCommands}

      cp "$extraDependenciesPath" "$out/extra-dependencies"

      ${config.system.extraSystemBuilderCmds}
    '';

  # Putting it all together.  This builds a store path containing
  # symlinks to the various parts of the built configuration (the
  # kernel, systemd units, init scripts, etc.) as well as a script
  # `switch-to-configuration' that activates the configuration and
  # makes it bootable. See `activatable-system.nix`.
  baseSystem = pkgs.stdenvNoCC.mkDerivation ({
    name = "argon-system-${config.system.name}-${config.system.nixos.label}";
    preferLocalBuild = true;
    allowSubstitutes = false;
    passAsFile = [ "extraDependencies" ];
    buildCommand = systemBuilder;

    systemd = config.systemd.package;

    nixosLabel = config.system.nixos.label;

    inherit (config.system) extraDependencies;
  } // config.system.systemBuilderArgs);

  escapeRegex = lib.escapeRegex or (escape (stringToCharacters "\\[{()^$?*+|."));
in {
  disabledModules = [
    "system/activation/switchable-system.nix"
  ];

  options = {
    argon = {
      allowedNixosActivationScripts = mkOption {
        type = types.listOf types.str;
      };

      activationScripts = mkOption {
        type = types.attrsOf types.unspecified;
        default = {};
      };

      build = {
        etc = mkOption {
          type = types.package;
        };

        system = mkOption {
          type = types.package;
        };
      };
    };
  };

  config.networking.hostName = mkDefault "argon";

  config.argon = {
    allowedNixosActivationScripts = [ "specialfs" "users" ];

    activationScripts.etc = stringAfter [ "users" ] ''
      echo Setting up /etc...
      ${pkgs.bash}/bin/bash ${./setup-etc.sh} "$systemConfig/etc/"
    '';

    build.etc = pkgs.stdenvNoCC.mkDerivation {
      name = "system-etc";
      preferLocalBuild = true;
      allowSubstitutes = false;
      src = config.system.build.etc;
      exclude = concatMapStringsSep "\n"
        (p: "^${escapeRegex p}")
        cfg.inhibit.etcSources;
      passAsFile = [ "buildCommand" "exclude" ];
      buildCommand = ''
        set -euo pipefail

        mkdir -p $out/etc

        copyOut() {
          local srcDir="$1"
          local dstDir="$2"

          while IFS= read -r -d ''' file; do
            local dest="$dstDir''${file:''${#srcDir}}"
            if [ -L "$file" ]; then
              if grep -qPf $excludePath < <(readlink "$file"); then
                : # Do not copy
              elif [ -d "$file" ]; then
                copyOut "$(readlink "$file")" "$dest"
              else
                mkdir -p "$(dirname "$dest")"
                cp -a "$file" "$dest"
              fi
            elif [ -d "$file" ]; then
              : # Directories created when required
            else
              mkdir -p "$(dirname "$dest")"
              cp -a "$file" "$dest"
            fi
          done < <(find $srcDir -mindepth 1 -print0)
        }

        copyOut "$src/etc" "$out/etc"

        # Remove broken symlinks
        find $out/etc/ -xtype l -delete
      '';
    };

    build.system = baseSystem;
  };

  config.systemd.tmpfiles.rules = [
    # Argon next system
    "L+ /nix/var/nix/gcroots/next-system - - - - /var/argon/next-system"
  ];

  # from nixos activation-script.nix
  config.system.activationScripts = {
    specialfs =
      let
        cond = mp: ''"$mountPoint" = ${escapeShellArg mp}'';
        mountFilter = concatMapStringsSep " -o " cond cfg.inhibit.specialMounts;
      in mkOverride 90 ''
        specialMount() {
          local device="$1"
          local mountPoint="$2"
          local options="$3"
          local fsType="$4"

          if [ ${mountFilter} ]; then
            return 0
          elif mountpoint -q "$mountPoint"; then
            local options="remount,$options"
          else
            mkdir -m 0755 -p "$mountPoint"
          fi
          mount -t "$fsType" -o "$options" "$device" "$mountPoint"
        }
        source ${config.system.build.earlyMountScript}
      '';
  };

  config.system.systemBuilderArgs.activationScript =
    let set = getAttrs cfg.allowedNixosActivationScripts config.system.activationScripts
      // mapAttrs (_: v: (if isString v then (noDepEntry v) else v) // { supportsDryActivation = false; }) cfg.activationScripts;
    in mkOverride 90 (systemActivationScript set false);

  config.systemd.services.argon-early = {
    wantedBy = [ "local-fs.target" ];
    before = [ "local-fs.target" ];
    conflicts = [ "shutdown.target" ];
    environment.PATH = mkForce "/usr/sbin:/usr/bin";
    unitConfig = {
      RequiresMountsFor = [ "/run" "/usr" "/nix/var" "/var" ];
      DefaultDependencies = "no";
    };
    serviceConfig = {
      Type = "oneshot";
      ExecStart = pkgs.writeScript "argon-early" ''
        #!${pkgs.bash}/bin/bash

        systemConfig="$(readlink /var/argon/next-system)"

        ${config.system.activationScripts.specialfs}
        ln -sfn "$systemConfig" /run/current-system
        ln -sfn "$systemConfig" /run/booted-system
      '';
    };
  };
}
