{ lib, ... }:

with lib;

let
  mockType = mkOptionType {
    name = "mock-option";
    description = "Mock option type used to disable options";
  };
  mockOption = attrs: mkOption ({ type = mockType; } // attrs);
  mockSubmodule = attrs: mkOption ({ type = types.submodule {}; } // attrs);
in {
  options = {
    boot.binfmt.registrations = mkOption {};
    boot.extraModulePackages = mockOption {};
    boot.extraSystemdUnitPaths = mockOption { default = []; };
    boot.initrd.availableKernelModules = mockOption {};
    boot.initrd.checkJournalingFS = mockOption { default = true; };
    boot.initrd.extraUdevRulesCommands = mockOption {};
    boot.initrd.kernelModules = mockOption {};
    boot.initrd.systemd = mockSubmodule {};
    boot.kernel.features = mockOption {};
    boot.kernelModules = mockOption {};
    boot.kernelPackages.kernel = mockOption { default = "4.19"; };
    boot.kernelParams = mockOption {};
    boot.kernelPatches = mockOption {};
    boot.loader.grub = mockOption {};
    boot.postBootCommands = mockOption {};
    boot.readOnlyNixStore = mkOption { default = false; };
    fonts.fontconfig.enable = mockOption {};
    networking.dhcpcd.denyInterfaces = mockOption {};
    networking.firewall = mockSubmodule {};
    networking.iproute2 = mockOption {};
    networking.resolvconf.enable = mkOption { default = false; };
    powerManagement.enable = mockOption { default = false; };
    powerManagement.resumeCommands = mockOption {};
    security.audit.enable = mockOption { default = false; };
    security.pam = mockOption {};
    security.polkit.enable = mockOption {};
    services.dbus = mockOption {};
    services.getty.helpLine = mockOption {};
    services.udisks2.enable = mockOption { default = false; };
    services.xserver.config = mockOption {};
    services.xserver.desktopManager.gnome.sessionPath = mockOption {};
    services.xserver.desktopManager.plasma5.enable = mockOption { default = false; };
    services.xserver.desktopManager.xfce.enable = mockOption { default = false; };
    services.xserver.display = mockOption { default = 0; };
    services.xserver.displayManager.hiddenUsers = mockOption {};
    services.xserver.drivers = mockOption {};
    services.xserver.enable = mockOption { default = false; };
    services.xserver.filesSection = mockOption {};
    services.xserver.inputClassSections = mockOption {};
    services.xserver.modules = mockOption {};
    services.xserver.screenSection = mockOption {};
    services.xserver.serverLayoutSection = mockOption {};
    services.xserver.startGnuPGAgent = mockOption {};
    services.xserver.videoDrivers = mockOption { default = []; };
    system.nssDatabases = mockOption {};
    system.nssModules = mockOption {};
    system.requiredKernelConfig = mockOption {};
    systemd.network.enabled = mockOption { default = false; };
    systemd.network.links = mockOption {};
    # From stage-1.nix
    fileSystems = mkOption {
      type = with lib.types; attrsOf (submodule {
        options.neededForBoot = mkOption {
          default = false;
          type = types.bool;
          description = lib.mdDoc ''
            If set, this file system will be mounted in the initial ramdisk.
            Note that the file system will always be mounted in the initial
            ramdisk if its mount point is one of the following:
            ${concatStringsSep ", " (
              forEach utils.pathsNeededForBoot (i: "{file}`${i}`")
            )}.
          '';
        };
      });
    };
  };

  disabledModules = [
    "config/console.nix"
    "config/fonts/fontconfig.nix"
    "config/i18n.nix"
    "config/iproute2.nix"
    "config/locale.nix"
    "config/nsswitch.nix"
    "config/power-management.nix"
    "config/pulseaudio.nix"
    "config/resolvconf.nix"
    "config/terminfo.nix"
    "config/xdg/icons.nix"
    "hardware/corectrl.nix"
    "hardware/cpu/amd-microcode.nix"
    "hardware/cpu/intel-microcode.nix"
    "hardware/video/amdgpu-pro.nix"
    "hardware/video/displaylink.nix"
    "hardware/video/hidpi.nix"
    "hardware/video/nvidia.nix"
    "hardware/video/webcam/facetimehd.nix"
    "installer/tools/tools.nix"
    "modules/virtualisation/parallels-guest.nix"
    "programs/command-not-found/command-not-found.nix"
    "programs/fuse.nix"
    "programs/gamemode.nix"
    "programs/pantheon-tweaks.nix"
    "programs/phosh.nix"
    "programs/steam.nix"
    "programs/sway.nix"
    "programs/x2goserver.nix"
    "security/audit.nix"
    "security/ca.nix"
    "security/google_oslogin.nix"
    "security/ipa.nix"
    "security/oath.nix"
    "security/pam_mount.nix"
    "security/pam_usb.nix"
    "security/pam.nix"
    "security/polkit.nix"
    "security/rtkit.nix"
    "security/sudo.nix"
    "security/sudo-rs.nix"
    "services/backup/btrbk.nix"
    "services/databases/clickhouse.nix"
    "services/desktops/flatpak.nix"
    "services/desktops/gnome/at-spi2-core.nix"
    "services/hardware/fwupd.nix"
    "services/hardware/udisks2.nix"
    "services/logging/klogd.nix"
    "services/misc/amazon-ssm-agent.nix"
    "services/misc/moonraker.nix"
    "services/misc/ssm-agent.nix"
    "services/misc/tzupdate.nix"
    "services/network-filesystems/ipfs.nix"
    "services/network-filesystems/kubo.nix"
    "services/network-filesystems/samba.nix"
    "services/networking/bind.nix"
    "services/networking/connman.nix"
    "services/networking/dhcpcd.nix"
    "services/networking/dnscrypt-wrapper.nix"
    "services/networking/dnsmasq.nix"
    "services/networking/fastnetmon-advanced.nix"
    "services/networking/firewall.nix"
    "services/networking/firewall-iptables.nix"
    "services/networking/firewall-nftables.nix"
    "services/networking/iwd.nix"
    "services/networking/iscsi/root-initiator.nix"
    "services/networking/kresd.nix"
    "services/networking/lokinet.nix"
    "services/networking/multipath.nix"
    "services/networking/nat-iptables.nix"
    "services/networking/nat-nftables.nix"
    "services/networking/netbird.nix"
    "services/networking/nftables.nix"
    "services/networking/networkmanager.nix"
    "services/networking/nixops-dns.nix"
    "services/networking/ntp/chrony.nix"
    "services/networking/ntp/ntpd.nix"
    "services/networking/ntp/openntpd.nix"
    "services/networking/tailscale.nix"
    "services/networking/twingate.nix"
    "services/networking/unbound.nix"
    "services/networking/wakeonlan.nix"
    "services/networking/wg-quick.nix"
    "services/networking/x2goserver.nix"
    "services/networking/xrdp.nix"
    "services/networking/zerotierone.nix"
    "services/printing/cupsd.nix"
    "services/printing/cups-pdf.nix"
    "services/printing/ipp-usb.nix"
    "services/security/usbguard.nix"
    "services/system/automatic-timezoned.nix"
    "services/system/cloud-init.nix"
    "services/system/dbus.nix"
    "services/system/nscd.nix"
    "services/ttys/getty.nix"
    "services/wayland/cage.nix"
    "services/web-apps/plausible.nix"
    "services/x11/desktop-managers/budgie.nix"
    "services/x11/desktop-managers/cinnamon.nix"
    "services/x11/desktop-managers/deepin.nix"
    "services/x11/desktop-managers/enlightenment.nix"
    "services/x11/desktop-managers/gnome.nix"
    "services/x11/desktop-managers/mate.nix"
    "services/x11/desktop-managers/pantheon.nix"
    "services/x11/desktop-managers/phosh.nix"
    "services/x11/desktop-managers/plasma5.nix"
    "services/x11/desktop-managers/xfce.nix"
    "services/x11/display-managers/gdm.nix"
    "services/x11/display-managers/lightdm.nix"
    "services/x11/display-managers/sddm.nix"
    "services/x11/display-managers/startx.nix"
    "services/x11/display-managers/sx.nix"
    "services/x11/display-managers/xpra.nix"
    "services/x11/extra-layouts.nix"
    "services/x11/redshift.nix"
    "services/x11/xserver.nix"
    "system/activation/specialisation.nix"
    "system/boot/binfmt.nix"
    "system/boot/grow-partition.nix"
    "system/boot/initrd-network.nix"
    "system/boot/initrd-openvpn.nix"
    "system/boot/initrd-ssh.nix"
    "system/boot/kernel.nix"
    "system/boot/kexec.nix"
    "system/boot/loader/external/external.nix"
    "system/boot/loader/generations-dir/generations-dir.nix"
    "system/boot/loader/generic-extlinux-compatible"
    "system/boot/loader/grub/grub.nix"
    "system/boot/loader/grub/ipxe.nix"
    "system/boot/loader/grub/memtest.nix"
    "system/boot/loader/loader.nix"
    "system/boot/loader/raspberrypi/raspberrypi.nix"
    "system/boot/loader/systemd-boot/systemd-boot.nix"
    "system/boot/luksroot.nix"
    "system/boot/networkd.nix"
    "system/boot/plymouth.nix"
    "system/boot/resolved.nix"
    "system/boot/shutdown.nix"
    "system/boot/stage-1.nix"
    "system/boot/stage-2.nix"
    "system/boot/systemd/initrd.nix"
    "system/boot/systemd/repart.nix"
    "system/boot/timesyncd.nix"
    "system/boot/uvesafb.nix"
    "tasks/bcache.nix"
    "tasks/encrypted-devices.nix"
    "tasks/filesystems/bcachefs.nix"
    "tasks/filesystems/btrfs.nix"
    "tasks/filesystems/cifs.nix"
    "tasks/filesystems/erofs.nix"
    "tasks/filesystems/ext.nix"
    "tasks/filesystems/f2fs.nix"
    "tasks/filesystems/jfs.nix"
    "tasks/filesystems/reiserfs.nix"
    "tasks/filesystems/unionfs-fuse.nix"
    "tasks/filesystems/vfat.nix"
    "tasks/filesystems/xfs.nix"
    "tasks/filesystems/zfs.nix"
    "tasks/lvm.nix"
    "tasks/network-interfaces-scripted.nix"
    "tasks/network-interfaces-systemd.nix"
    "tasks/swraid.nix"
    "tasks/trackpoint.nix"
    "virtualisation/anbox.nix"
    "virtualisation/appvm.nix"
    "virtualisation/container-config.nix"
    "virtualisation/libvirtd.nix"
    "virtualisation/parallels-guest.nix"
    "virtualisation/virtualbox-guest.nix"
    "virtualisation/virtualbox-host.nix"
    "virtualisation/vmware-host.nix"
    "virtualisation/xen-dom0.nix"
  ];

  # Does not build with disabled modules
  config.documentation.nixos.enable = false;
}
