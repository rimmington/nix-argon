{
  nixosModulesPath ? <nixpkgs/nixos/modules>
}:

rec {
  nixosModules = import "${builtins.toPath nixosModulesPath}/module-list.nix";

  argonModules = [
    argon/activation.nix
    argon/apt-decl.nix
    argon/i18n.nix
    argon/inhibit.nix
    argon/mock.nix
    argon/path.nix
  ];

  allModules = nixosModules ++ argonModules;
}
