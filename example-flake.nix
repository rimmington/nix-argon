{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
  inputs.argon.url = "gitlab:rimmington/nix-argon";

  outputs = { nixpkgs, argon, ... }: {
    argonConfigurations.hostname = argon.lib.argonConfiguration {
      pkgs = import nixpkgs {
        system = "x86_64-linux";
      };
      modules = [ /path/to/argon-config.nix ];
    };
  };
}
